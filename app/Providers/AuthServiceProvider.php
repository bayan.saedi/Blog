<?php

namespace App\Providers;

use App\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Post' => 'App\Policies\PostPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        foreach ($this->getPermission() as $permission) {
            Gate::define($permission->name, function($user) use($permission) {
                return $user->hasRole($permission->roles);
            });
        }        

        // Gate::define('update-post', function($user, $post) {
        //     return $user->id == $post->user_id;                        
        // }); 

    }

    protected function getPermission()
    {
        return Permission::with('roles')->get();
    }

}
