<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function publish(Post $post)
    {
        $this->posts()->save($post);
    }

    public function addCommentOn(Post $post, $body)
    {
        return (new Comment(compact('body')))
            ->user()->associate($this)
            ->post()->associate($post)
            ->save();
    }

    public function hasRole($role)   
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !!$role->intersect($this->roles)->count();

        // foreach ($role as $r) {
        //     if ($this->hasRole($r->name)) {
        //         return true;
        //     }
        // }
    }

}
