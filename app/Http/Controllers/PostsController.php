<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostsRequest;
use App\Post;
use Gate;


class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function index()
    {
        $posts = Post::latest()->filter(request(['month', 'year']))->paginate(2);


    	return view('posts.index', compact('posts', 'archives'));
    }

    public function show(Post $post)
    {
    	return view('posts.show', compact('post'));
    }

    public function create()
    {
    	return view('posts/create');
    }

    public function store(PostsRequest $requst)
    {

        auth()->user()->publish(
            new Post(request(['title', 'body']))
        );

        session()->flash('message', 'Your post has been created successfully');

        return redirect('/');

    }

    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    public function update(Post $post)
    {
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required'
        ]);

        // if (auth()->user()->canNot('update', $post)) {
        //     abort('403', 'You are not allowed this action');
        // } 

        if (Gate::denies('update', $post)) {
            abort('403', 'Sorry you cant');
        }

        // $this->authorize('update', $post);

        // if ($post->user->id !== auth()->id()) {
        //     abort('403', 'You are not allowed');
        // }

        $post->title = request('title');
        $post->body = request('body');
        $post->save(); 

        return redirect()->home();
    }

    public function destroy(Post $post)
    {   
        $post->comments()->delete();

        $tags = $post->tags;
        $post->tags()->detach($tag);

        $post->delete();   

        return redirect()->home();
    }
}
