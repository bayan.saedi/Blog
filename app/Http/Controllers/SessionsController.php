<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
	public function __construct()
	{
		$this->middleware('guest')->except('destroy');
	}

    public function destroy()
    {
    	auth()->logout();

    	return redirect()->home();
    }

    public function create()
    {
    	return view('sessions.login');
    }

    public function store()
    {
    	$this->validate(request(), [
    		'email' => 'required|email',
    		'password'  => 'required'
    	]);

        $remember = !is_null(request('remember'));

    	if (!auth()->attempt(request(['email', 'password']), $remember)) {
    		return back()->withErrors(['message' => 'Please confirm your credential and try again']);
    	}

    	return redirect()->home();
    }
}
