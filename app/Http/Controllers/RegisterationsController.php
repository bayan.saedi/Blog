<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail\WelcomeAgain;
use App\Http\Requests\RegisterValidation;

class RegisterationsController extends Controller
{
    public function create()
    {
    	return view('register.create');
    }

    public function store(RegisterValidation $request)
    {
        $request->persist();

        session()->flash('message', 'You have been signed up successfully');

		return redirect()->home();

    }

    public function update($confirmationCode)
    {
        if (!confirmationCode) {
            throw new Exception("You can not use this website. please first verify your email");
        }

        $user = User::where('confirmation_code', $confirmationCode)->first();

        $user->confirmation_code = null;
        $user->confirmed = 1;
        $user->save();

        session()->flash('messsage', 'Your account has been verified!. Please log in to your account.');

        return redirect()->route('login');

    }
}
