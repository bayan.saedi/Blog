<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
use App\Mail\WelcomeAgain;

class RegisterValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:4'
        ];
    }

    public function persist()
    {
        $confimationCode = str_random();

        $user = User::create([
            'name' => $this->input('name'),
            'email' => $this->input('email'),
            'password' => bcrypt($this->input('password')),
            $confirmation_code = $confimationCode;
        ]);
        

        auth()->login($user);

        \Mail::to($user)->send(new WelcomeAgain($user));
    }
}
