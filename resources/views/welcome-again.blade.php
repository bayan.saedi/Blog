@component('mail::message')
# My new course about PHP is released

## ALl Parts in this Tutorial

- PHP MVC
- Routing
- Queue

The body of your message.

@component('mail::button', ['url' => 'https://www.jabama.ir'])
Go TO Course
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
