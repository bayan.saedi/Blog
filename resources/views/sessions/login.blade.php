@extends('layouts.master')

@section('content')
	<div class="col-sm-8 blog-main">
		
		<h2>Login Form</h2>
		<hr>

		<form action="/login" method="POST">
			
			{{ csrf_field() }}

			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" name="email" id="email" class="form-control">
			</div>

			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" name="password" id="password" class="form-control">
			</div>

			<label class="custom-control custom-checkbox">
				<input type="checkbox" class="custom-control-input" name="remember">
				<span class="custom-control-indicator"></span>
				<span class="custom-control-description">Remember Me</span>
			</label>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Login</button>
			</div>

			@include('layouts.errors')
			
		</form>
	</div>
@stop