@extends('layouts.master')

@section('content')
	<div class="col-sm-8 blog-main">		
		<h1>Create a New Post</h1>

		<hr>

		<form action="/posts" method="POST" class="mb-2">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" name="title" id="title" class="form-control" required>
			</div>

			<div class="form-group">
				<label for="body">Body of your post</label>
				<textarea name="body" id="body" class="form-control" minlength="6"></textarea>
			</div>

			<button type="submit" class="btn btn-primary">Publish</button>
		</form>

		@include('layouts.errors')
		
	</div>
@stop

@section('script')
	<script src="{{ URL::to("js/vendor/jquery-3.1.0.min.js") }}"></script>
	<script src="{{ URL::to("js/vendor/tinymce/tinymce.min.js") }}"></script>
	{{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> --}}
	<script>
	  var editor_config = {
	    path_absolute : "/",
	    selector: "textarea",
	    plugins: [
	      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
	      "searchreplace wordcount visualblocks visualchars code fullscreen",
	      "insertdatetime media nonbreaking save table contextmenu directionality",
	      "emoticons template paste textcolor colorpicker textpattern"
	    ],
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
	    relative_urls: false,
	    file_browser_callback : function(field_name, url, type, win) {
	      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
	      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

	      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
	      if (type == 'image') {
	        cmsURL = cmsURL + "&type=Images";
	      } else {
	        cmsURL = cmsURL + "&type=Files";
	      }

	      tinyMCE.activeEditor.windowManager.open({
	        file : cmsURL,
	        title : 'Filemanager',
	        width : x * 0.8,
	        height : y * 0.8,
	        resizable : "yes",
	        close_previous : "no"
	      });
	    }
	  };

	  tinymce.init(editor_config);
	</script>

@endsection