<div class="blog-post">

    <h2 class="blog-post-title">
    	<a href="/posts/{{ $post->id }}">
    		{{ $post->title }}
    	</a>
    </h2>

    <p class="blog-post-meta">{{ jDate($post->created_at)->ago() }} by {{ $post->user->name }}</p>

    {!! $post->body !!}
</div><!-- /.blog-post -->

