@extends('layouts.master')

@section('content')

	<div class="col-sm-8 blog main">
		<h1>Update Post</h1>
		<hr>

		<form data-parsley-validate action="/posts/{{ $post->id }}/update" method="POST">
		{{ csrf_field() }}
		{{ method_field('PATCH') }}
			
			<div class="form-group">
				<label for="title">Title:</label>
				<input type="text" name="title" id="title" class="form-control" value="{{ $post->title }}" required>
			</div>

			<div class="form-group">
				<label for="body">Body:</label>
				<textarea name="body" id="body" class="form-control" minlength="6">{{ $post->body }}
				</textarea>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			
		</form>

		@include('layouts.errors')
	</div>

@endsection