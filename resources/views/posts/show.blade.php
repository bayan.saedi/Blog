@extends('layouts.master')

@section('content')
	<div class="col-sm-8 blog-main">
		<div class="d-flex">
			<h1>{{ $post->title }}</h1>

			<div class="d-flex ml-auto">
			@can('edit_post')
			@can('update', $post)
				<a href="/posts/{{ $post->id }}/edit" class="btn btn-success btn-sm">
					Edit
				</a>
			@endcan
			@endcan
				
				<form action="/posts/{{ $post->id }}" method="POST">
					{{ method_field('DELETE') }}
					{{ csrf_field() }}
					<button type="submit" class="btn btn-danger btn-sm">delete</button>
					
				</form>
			</div>
		</div>

		<div>
			{!! $post->body !!}
		</div>

		@foreach ($post->tags as $tag)
			{{ $tag->name }}
		@endforeach

		<div class="card">
			<div class="card-block">
				<form action="/posts/{{ $post->id }}/comments" method="POST">

					{{ csrf_field() }}
					<div class="form-group">
						<textarea name="body" id="body" class="form-control" placeholder="Add your comment"></textarea>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary">Publish</button>
					</div>

				</form>

				@include('layouts.errors')
				<ul class="list-group">
					@foreach ($post->comments as $comment)
						<li class="list-group-item">
							<strong>
								{{ jDate($comment->created_at)->ago() }}: &nbsp;
							</strong>
							{{ $comment->body }}
						</li>
					@endforeach
				</ul>

			</div>
		</div>
	</div>
@stop