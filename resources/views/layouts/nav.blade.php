<div class="blog-masthead">
  <div class="container">
    <nav class="nav blog-nav">
      <a class="nav-link active" href="#">Home</a>
      <a class="nav-link" href="#">New features</a>
      <a class="nav-link" href="#">Press</a>
      <a class="nav-link" href="#">New hires</a>
      <a class="nav-link" href="/posts/create">Create a New Post</a>
      @if (Auth::guest())
          <a class="nav-link ml-auto" href="{{ route('login') }}">Login</a>
          <a class="nav-link" href="{{ route('register') }}">Register</a>
      @else
      	<a class="nav-link ml-auto" href="#">
      		{{ auth()->user()->name }}
      	</a>
        <a class="nav-link" href="{{ route('logout') }}">Logout</a>
      @endif
      
    </nav>
  </div>
</div>