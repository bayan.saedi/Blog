@if (count($errors))
	<ul class="alert alert-danger">
		@foreach ($errors->all() as $error)
			<li @if(App::getLocale() == 'fa') style="direction: rtl;" @endif>{{ $error }}</li>
		@endforeach
	</ul>
@endif