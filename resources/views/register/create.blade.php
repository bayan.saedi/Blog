@extends('layouts.master')

@section('content')
	<div class="col-md-8 blog-main">
		<h2>Sign Up</h2>

		<hr>

		<form action="/register" method="POST">
			{{ csrf_field() }}
			
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" name="name" id="name" class="form-control">
			</div>

			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" name="email" id="email" class="
				form-control">
			</div>

			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" name="password" id="password" class="form-control">
			</div>

			<div class="form-group">
				<label for="password_confirmation">Confirm Your Password:</label>
				<input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
			</div>

			<div class="form-group">
				<button class="btn btn-primary">Save</button>
			</div>
			
		</form>

		@include('layouts.errors')
	</div>
@stop