<?php


// Route::get('/', function() {
// 	dd(app('Illuminate\Contracts\Config\Repository'));
// });

Route::get('/', 'PostsController@index')->name('home');
Route::get('/posts/create', 'PostsController@create');
Route::get('/posts/{post}', 'PostsController@show');
Route::post('/posts', 'PostsController@store');
Route::get('/posts/{post}/edit', 'PostsController@edit');
Route::patch('/posts/{post}/update', 'PostsController@update');
Route::delete('/posts/{post}', 'PostsController@destroy');


Route::post('/posts/{post}/comments', 'CommentsController@store');

Route::get('login', 'SessionsController@create')->name('login');
Route::post('login', 'SessionsController@store');
Route::get('logout', 'SessionsController@destroy')->name('logout');

Route::get('register', 'RegisterationsController@create')->name('register');
Route::post('register',  'RegisterationsController@store');
// Route::get('register/verify/{confirmationCode}', 'RegisterationsController@update');

Route::get('dashboard', function() {
	return 'dashboard';
});

Route::get('posts/tags/{tag}', 'TagsController@show');